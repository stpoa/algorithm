const Client = require('mariasql')
const faker = require('faker')
const json = require('express-json')
const express = require('express')
/**
 * Create rest HTTP server
 * 
 * @param {any} req
 * @param {any} res
 */
const app = express().use(json()).use(function (req, res) {
    res.json(scheduleProfessors);
  })
  .listen(3000);

/**
 * Group class constructor
 * 
 * @param {Number} id
 */
function Group (id) {
	this.id = id // Number
}

/**
 * Professor class constructor
 * 
 * @param {String} name
 * @param {Array} courses
 */
function Professor (name, courses) {
	this.name = name // String
  this.courses = courses // [String]
}

/**
 * Room class constructor
 * 
 * @param {Number} id
 */
function Room (id) {
	this.id = id // Number
  this.occupied = [[false]] // Array od arrays of bools (Days / Hours)
}

/**
 * Course class constructor
 * 
 * @param {String} name
 */
function Course (name) {
	this.name = name // String
}

/**
 * Schedule event constructor
 * 
 * @param {Name} id
 * @param {Room} classroom
 * @param {Course} lesson
 * @param {Group} group
 * @param {Lecturer} lecturer
 * @param {Number} timeStart
 * @param {Number} timeEnd
 */
function Event (id, classroom, lesson, group, lecturer, timeStart, timeEnd) {
	this.id = id
  this.classroom = classroom
  this.lesson = lesson
  this.group = group
  this.lecturer = lecturer
  this.timeStart = timeStart
  this.timeEnd = timeEnd
}

/**
 * Returns if room is occupied
 * 
 * @param {Number} day
 * @param {Number} hour
 * @returns {Bool}
 */
Room.prototype.isOccupied = function (day, hour) {
  return this.occupied ? this.occupied[day] && this.occupied[day][hour] : false
}

const courseNames = ['Matematyka', 'Plastyka', 'Przyroda', 'WF', 'Analiza Matematyczna', 'Inżynieria Oprogramowania']

// Generate coursers
const courses = courseNames.map((name) => new Course(name))
const professors = [...Array(10)].map((v, i) => new Professor(faker.name.findName(), courses[i % (courses.length - 1)]))
const rooms = [...Array(10)].map((v, i) => new Room(i))
const groups = [...Array(3)].map((v, i) => new Group(i))

// Preferences
const hourCount = 12

// Generate schedule
const scheduleProfessors = [...Array(5)]
	.map((day, j) => [...Array(hourCount)].map((hour, i) => {
		const room = rooms
    	.find((r) => r.isOccupied(day, hour))
    const courseIndex = Math
      .round(Math.random() % (courses.length - 1))
  	const course = courses[courseIndex]
  	const group = groups[j*i % (groups.length - 1)]
		return (
      new Event(i, room, course, group, professors[j*i % (professors.length - 1)], i, i+1)
    )
	})
)

// Connect to DB
const c = new Client({
  host: 'localhost',
  user: 'root',
  password: 'studia',
  db: 'dziekanat'
})

// Query DB
c.query('SELECT * FROM add_subject', function(err, rows) {
  if (err) throw err
  console.log("Select all subjects")
  console.dir(rows)
})
c.query('SELECT * FROM add_lecturer', function(err, rows) {
  if (err) throw err
  console.log("Select all lecturer")
  console.dir(rows)
})
c.query('SELECT * FROM add_class', function(err, rows) {
  if (err) throw err
  console.log("Select all class")
  console.dir(rows)
})
c.end()